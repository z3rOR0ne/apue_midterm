<img src="assets/apue_midterm_image">


# bls
### a bare bones reimplementation of the GNU ls command

This is a basic implementation of the GNU ls command, rewritten in C by a complete beginner, that's me!

This project was done while following along with the CS631 course from Stevens University, taught by J Schauma ([link](https://stevens.netmeister.org/631/f20-midterm.html)).

NOTE: This project is currently left incompleted (Snapshot implementation) per the course requirements. I am not participating in the class, but simpy following along with the lecture and overviewing the notes and assignments, while learning more advanced topics related to the Unix Environment and Unix Programming.

I may return to this project from time to time to see if I can implement further features that would make it more closely resemble the GNU ls command, but am currently abandoning this project to pursue others.

To install simply:

`git clone https://codeberg.org/z3rOR0ne/bls`

`cd bls`

`make install`

To uninstall:

`make clean`

