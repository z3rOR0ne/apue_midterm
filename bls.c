#include "bls.h"

DIR *dp;
struct dirent *dirp;
char *filename;
int r, length, opt;
int n = 0;
int i = 0;
int aFlag = 0;
int iFlag = 0;
int lFlag = 0;
struct stat fs;

int main(int argc, char *argv[]) {
    if (argc == 1) {
        lsHere();
        return 0;
    }

    if (strstr(argv[1], "-") != NULL) {
        while ((opt = getopt(argc, argv, "lai")) != -1) {
            switch (opt) {
                case 'l':
                    lFlag = 1;
                    if (optind == 1 && strstr(argv[1], "-la") &&
                        !strstr(argv[1], "-lai") && !strstr(argv[1], "-lia")) {
                        aFlag = 1;
                        lloop(argc, argv);
                        break;
                    } else if (optind == 1 && strstr(argv[1], "-li") &&
                               !strstr(argv[1], "-lia")) {
                        iFlag = 1;
                        lloop(argc, argv);
                        break;
                    } else if ((optind == 1 && strstr(argv[1], "-lia")) ||
                               (optind == 1 && strstr(argv[1], "-lai"))) {
                        aFlag = 1;
                        iFlag = 1;
                        lloop(argc, argv);
                        break;
                    } else {
                        lloop(argc, argv);
                        break;
                    }
                case 'a':
                    aFlag = 1;
                    if (optind == 1 && strstr(argv[1], "-al") &&
                        !strstr(argv[1], "-ali") && !strstr(argv[1], "-ail")) {
                        lloop(argc, argv);
                        break;
                    } else if (optind == 1 && strstr(argv[1], "-ai") &&
                               !strstr(argv[1], "-ail")) {
                        iFlag = 1;
                        chdir(argv[2]);
                        lsHere();
                        break;
                    } else if ((optind == 1 && strstr(argv[1], "-ali")) ||
                               (optind == 1 && strstr(argv[1], "-ail"))) {
                        iFlag = 1;
                        lloop(argc, argv);
                        break;
                    } else {
                        chdir(argv[2]);
                        lsHere();
                        break;
                    }
                case 'i':
                    iFlag = 1;
                    if (optind == 1 && strstr(argv[1], "-il") &&
                        !strstr(argv[1], "-ila")) {
                        lloop(argc, argv);
                        break;
                    } else if (optind == 1 && strstr(argv[1], "-ia") &&
                               !strstr(argv[1], "-ial")) {
                        aFlag = 1;
                        chdir(argv[2]);
                        lsHere();
                        break;
                    } else if ((optind == 1 && strstr(argv[1], "-ila")) ||
                               (optind == 1 && strstr(argv[1], "-ial"))) {
                        aFlag = 1;
                        lloop(argc, argv);
                        break;
                    } else {
                        chdir(argv[2]);
                        lsHere();
                        break;
                    }
                case '?': {
                    printf("optind is: %d\n", optind);
                    printf("optchar is: %s\n", optarg);
                    return 1;
                }
            }
        }
    } else if (argc == 2 && isReg(argv[1])) {
        printf("%s\n", argv[1]);
        return 0;
    } else if (argc == 2 && isDir(argv[1])) {
        chdir(argv[1]);
        lsHere();
        return 0;
    } else if (ENOENT == errno) {
        printf("\"%s\": No such file or directory (os error 2)\n", argv[1]);
        return 1;
    }
    return 0;
}
