#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

extern DIR *dp;
extern struct dirent *dirp;
extern char *filename;
extern int r, length, opt;
extern int n;
extern int i;
extern int aFlag;
extern int iFlag;
extern int lFlag;
extern struct stat fs;

int isDir(char *argv);

int isReg(char *argv);

int lsHere();

void lloop(int argc, char *argv[]);
