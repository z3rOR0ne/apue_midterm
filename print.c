#include "bls.h"

int isDir(char *argv) {
    struct stat buf;
    lstat(argv, &buf);
    return S_ISDIR(buf.st_mode);
}

int isReg(char *argv) {
    struct stat buf;
    lstat(argv, &buf);
    return S_ISREG(buf.st_mode);
}

static void countElems() {
    while ((dirp = readdir(dp)) != NULL) {
        if (!strcmp(dirp->d_name, ".") || !strcmp(dirp->d_name, "..")) {
        } else {
            n++;
        }
    }
    rewinddir(dp);
}

static void findDir(int argc, char *argv[]) {
    if (argc == 1) {
        dp = opendir(".");
    }
    if (argc == 2 && isDir(argv[1])) {
        dp = opendir(argv[1]);
        chdir(argv[1]);
    }

    if (argc == 2) {
        dp = opendir(".");
    }
    if (argc == 3 && isDir(argv[2])) {
        dp = opendir(argv[2]);
        chdir(argv[2]);
    }
}

// compares the first character of a string to determine if it is a '.', returns
// true
static int isAbsolute(const char *str) { return (str[0] == '.'); }

int lsHere() {
    struct dirent **dirp;
    int scandirat();
    int count;

    count = scandir("./", &dirp, NULL, alphasort);
    for (int z = 0; z < count; z++) {
        if (!strcmp(dirp[z]->d_name, ".") || !strcmp(dirp[z]->d_name, "..")) {
        } else if (aFlag == 0 && isAbsolute(dirp[z]->d_name)) {
        } else if (iFlag == 1) {
            printf("%lu ", dirp[z]->d_ino);
            printf("%s\n", dirp[z]->d_name);
        } else
            printf("%s\n", dirp[z]->d_name);
    }
    return 0;
}

static void chopN(char *str, size_t n) {
    assert(n != 0 && str != 0);
    size_t len = strlen(str);
    if (n > len) return;
    memmove(str, str + n, len - n + 1);
}

static void allocateList(char *filesList[n]) {
    while ((dirp = readdir(dp)) != NULL) {
        if (!strcmp(dirp->d_name, ".") || !strcmp(dirp->d_name, "..")) {
        } else {
            filesList[i] = calloc(strlen(dirp->d_name) + 1, 30 * sizeof(char));
            if (*filesList[i] >= 0) strcpy(filesList[i], dirp->d_name);
            i++;
        }
    }
}

static void alphabetizeList(char *filesList[n]) {
    char temp[1000];
    for (int k = 0; k < n; k++) {
        for (int l = 0; l < n - 1 - k; l++) {
            if (strcmp(filesList[l], filesList[l + 1]) > 0) {
                strcpy(temp, filesList[l]);
                strcpy(filesList[l], filesList[l + 1]);
                strcpy(filesList[l + 1], temp);
            }
        }
    }
}

static void lFlagLoop(char *filesList[n], int filesListLength) {
    // copies filesList to new array that excludes dotfiles
    char *noDotFilesList[n];
    for (int y = 0; y < filesListLength; y++) {
        if (!isAbsolute(filesList[y])) {
            noDotFilesList[y] = filesList[y];
        }
    }

    for (int z = 0; z < filesListLength; z++) {
        if (aFlag == 1)
            filename = filesList[z];
        else
            filename = noDotFilesList[z];

        r = stat(filename, &fs);

        if (r == -1) {
            continue;
        }

        char *t = ctime(&fs.st_mtime);
        if (t[strlen(t) - 1] == '\n') t[strlen(t) - 9] = '\0';
        chopN(t, 4);

        struct passwd *pwd = getpwuid(fs.st_uid);
        struct group *grp = getgrgid(fs.st_gid);

        if (iFlag == 1) {
            printf("%lu ", fs.st_ino);
        }

        if (S_ISREG(fs.st_mode))
            printf("-");
        else if (S_ISDIR(fs.st_mode))
            printf("d");
        else if (S_ISFIFO(fs.st_mode))
            printf("p");
        else if (S_ISLNK(fs.st_mode))
            printf("l");
        else if (S_ISBLK(fs.st_mode))
            printf("b");
        else if (S_ISCHR(fs.st_mode))
            printf("c");
        else if (S_ISSOCK(fs.st_mode))
            printf("s");

        if (fs.st_mode & S_IRUSR)
            printf("r");
        else
            printf("-");
        if (fs.st_mode & S_IWUSR)
            printf("w");
        else
            printf("-");
        if (fs.st_mode & S_IXUSR)
            printf("x");
        else
            printf("-");
        if (fs.st_mode & S_IRGRP)
            printf("r");
        else
            printf("-");
        if (fs.st_mode & S_IWGRP)
            printf("w");
        else
            printf("-");
        if (fs.st_mode & S_IXGRP)
            printf("x");
        else
            printf("-");
        if (fs.st_mode & S_IROTH)
            printf("r");
        else
            printf("-");
        if (fs.st_mode & S_IWOTH)
            printf("w");
        else
            printf("-");
        if (fs.st_mode & S_IXOTH)
            printf("x");
        else
            printf("-");
        putchar(' ');

        printf("%lu", fs.st_nlink);
        putchar(' ');
        printf("%s %s", pwd->pw_name, grp->gr_name);
        putchar(' ');
        printf("%lu", fs.st_size);
        putchar(' ');
        printf("%s %s", t, filename);
        printf("\n");
    }
}

void lloop(int argc, char *argv[]) {
    findDir(argc, argv);
    countElems();

    char *filesList[n];

    allocateList(filesList);
    alphabetizeList(filesList);

    int filesListLength = sizeof filesList / sizeof filesList[0];
    lFlagLoop(filesList, filesListLength);

    rewinddir(dp);
    closedir(dp);
    free(*filesList);
    exit(0);
}
